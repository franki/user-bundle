<?php

namespace Polargold\UserBundle;

use Polargold\UserBundle\DependencyInjection\Compiler\FOSUserPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PolargoldUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new FOSUserPass());
    }

    public function getParent()
    {
       return 'FOSUserBundle';
    }
}
