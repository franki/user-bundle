<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 30.03.15
 * Time: 21:33
 */

namespace Polargold\UserBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FOSUserPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        $container->getDefinition('fos_user.listener.authentication')->setClass(
            'Polargold\UserBundle\EventListener\CustomAuthenticationListener'
        );
    }

}