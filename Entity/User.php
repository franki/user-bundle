<?php

namespace Polargold\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as FOSUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Polargold\UserBundle\Entity\UserRepository")
 */
class User extends FOSUser
{
    protected $_availableRoles = array(
        'ROLE_CREATION' => 'ROLE_CREATION',
        'ROLE_PM'       => 'ROLE_PM',
        'ROLE_DEV'      => 'ROLE_DEV'
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="cruser_id", type="integer", nullable=true)
     */
    private $cruserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="aluser_id", type="integer", nullable=true)
     */
    private $aluserId;

    public function __construct()
    {
        parent::__construct();
    }

    public function getAvailableRoles()
    {
        return $this->_availableRoles;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set cruserId
     *
     * @param integer $cruserId
     *
     * @return User
     */
    public function setCruserId($cruserId)
    {
        $this->cruserId = $cruserId;

        return $this;
    }

    /**
     * Get cruserId
     *
     * @return integer
     */
    public function getCruserId()
    {
        return $this->cruserId;
    }

    /**
     * Set aluserId
     *
     * @param integer $aluserId
     *
     * @return User
     */
    public function setAluserId($aluserId)
    {
        $this->aluserId = $aluserId;

        return $this;
    }

    /**
     * Get aluserId
     *
     * @return integer
     */
    public function getAluserId()
    {
        return $this->aluserId;
    }
}
