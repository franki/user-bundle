How to install Polargold  UserBundle
------------------------------------



1) install via composer -- https://getcomposer.org/doc/05-repositories.md#using-private-repositories
 
 

add to composer.json

    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/polargold/user-bundle.git"
        }
    ],
    
and under the require section:

    "franki/user-bundle": "dev-master"




2) import security.yml, fos_user.yml if you want to use defaults


     - { resource: @PolargoldUserBundle/Resources/config/security.yml }
     - { resource: @PolargoldUserBundle/Resources/config/fos_user.yml }



3) Import routing file in routing.yml:

    polargold_user_bundle:
        resource: "@PolargoldUserBundle/Resources/config/routing.yml"

4) Add Bundle to App kernel:
    ...
    new Polargold\UserBundle\PolargoldUserBundle(),
    ...


5) Update database

