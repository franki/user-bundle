<?php

namespace Polargold\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Polargold\UserBundle\Entity\User;

class RegistrationFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = new User();
        $builder
            ->add('name')
            ->add(
                'roles',
                'choice',
                array(
                    'choices'  => $user->getAvailableRoles(),
                    'multiple' => true
                )
            );
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'polargold_user_registration';
    }
}
