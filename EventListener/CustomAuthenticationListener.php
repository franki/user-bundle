<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Polargold\UserBundle\EventListener;

use FOS\UserBundle\EventListener;
use FOS\UserBundle\FOSUserEvents;

class CustomAuthenticationListener extends EventListener\AuthenticationListener
{
    /**
     * @desc Simply overwrite events that the listener listens to.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRMED    => 'authenticate',
            FOSUserEvents::RESETTING_RESET_COMPLETED => 'authenticate',
        );
    }
}
