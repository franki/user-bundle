<?php

namespace Polargold\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();


        //login because we access admin area



        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/user/');

        // redirects to the login page
        $crawler = $client->followRedirect();


        // submits the login form
        $form = $crawler->selectButton('Login')->form(array('_username' => 'test', '_password' => 'test'));
        $client->submit($form);

        // redirect to the original page (but now authenticated)
        $crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /admin/user/");
        $crawler = $client->click($crawler->selectLink('Create a new entry')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
                'polargold_common_userbundle_user[username]' => 'Test',
                'polargold_common_userbundle_user[name]'     => 'Test',
                'polargold_common_userbundle_user[email]'    => 'Test@mail.com',
                'polargold_common_userbundle_user[password]' => 'Test',
                'polargold_common_userbundle_user[roles]'    => '3',
            // ... other fields to fill
        ));

        $client->submit($form);

        //var_dump($client->getResponse()->getContent());
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Edit')->form(array(
                'polargold_common_userbundle_user[username]' => 'Test123',
                'polargold_common_userbundle_user[name]'     => 'Test123',
                'polargold_common_userbundle_user[email]'    => 'Test123@mail.com',
                'polargold_common_userbundle_user[password]' => 'Test123',
                'polargold_common_userbundle_user[roles]'    => '3',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(
            0,
            $crawler->filter('[value="Test123"]')->count(),
            'Missing element [value="Test123"]'
        );

        // Delete the entity
        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }

}
