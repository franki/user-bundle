Polargold UserBundle
=========================

This user bundle extends the well known FOS User Bundle to provide a pre configured admin interface for easy set up


Check out the [doc](Resources/doc) directory for documentation


Install guide
-------------

[install.md](Resources/doc/install.md)
